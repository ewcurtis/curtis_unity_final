﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathTrigger : MonoBehaviour
{

    private GameObject Player;
    private Animator anim;

    void Awake()
    {
        Player = GameObject.FindGameObjectWithTag("PC");
        anim = Player.GetComponent<Animator>();

    }

    //kills player contact and reloads scene
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            anim.SetTrigger("Die");
            Invoke("Reload", 2.3f);
            //SceneManager.LoadScene("SampleScene");
        }
    }

    //reloads level
    void Reload()
    {
        SceneManager.LoadScene("SampleScene");

    }
}
