﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace circus
{
    public class damage_player : MonoBehaviour
    {

        private PlayerHealth ph;
        public int dmg = 1;
        private GameObject player;
        private GameObject damager;
        private GameObject mover;
        private GameObject healthUI;
        private EnemyHealth eh;
        private Animator pAnim;
        bool playerInRange;
        float timer;
        float timeBetweenAttacks = 1f;


        // Start is called before the first frame update
        void Awake()
        {
            player = GameObject.FindGameObjectWithTag("PC");
            mover = GameObject.FindGameObjectWithTag("Player");
            ph = player.GetComponent<PlayerHealth>();
            damager = GetComponent<GameObject>();
            eh = GetComponent<EnemyHealth>();
            pAnim = player.GetComponent<Animator>();
            healthUI = GameObject.FindGameObjectWithTag("HealthUI");
        }

        // Update is called once per frame
        void Update()
        {
            // Add the time since Update was last called to the timer.
            timer += Time.deltaTime;

            string trigger = "health" + ph.currentHealth.ToString();

            healthUI.GetComponent<Animator>().SetTrigger(trigger);

            //checks conditions for enemies
            if (eh)
            {
                // If the timer exceeds the time between attacks, the player is in range and this enemy is alive...
                if (timer >= timeBetweenAttacks && playerInRange && eh.currentHealth > 0)
                {
                    // ... attack.
                    Attack();
                }
            }
            else
            {
                //checks conditions for hazards
                if (timer >= timeBetweenAttacks && playerInRange)
                {
                    Attack();
                }
            }
            // If the player has zero or less health...
            if (ph.currentHealth <= 0)
            {
                // ... tell the animator the player is dead.
                //pAnim.SetTrigger("Die");
            }
        }

        //the trigger functions ensure the player is only damage when they make constact with the enemy.
        void OnTriggerEnter2D(Collider2D other)
        {

            if (other.gameObject == mover)
            {
                playerInRange = true;
            }

        }

        void OnTriggerExit2D(Collider2D other)
        {
            if (other.gameObject == mover)
            {
                playerInRange = false;
            }
        }

        void Attack()
        {
            // Reset the timer.
            timer = 0f;

            // If the player has health to lose...
            if (ph.currentHealth > 0)
            {
                // ... damage the player.
                ph.TakeDamage(dmg);
            }
        }
    }
}