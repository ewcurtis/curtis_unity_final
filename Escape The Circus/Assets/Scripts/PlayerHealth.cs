﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace circus
{
    public class PlayerHealth : MonoBehaviour
    {

        public int startingHealth = 5;
        public int currentHealth;

        bool damaged;
        bool isDead;
        private Player_Movement pm;

        Animator anim;
        AudioSource sfx;
        public AudioClip playerHurt;
        public AudioClip playerDeath;

        // Start is called before the first frame update
        void Awake()
        {
            currentHealth = startingHealth;
            anim = GetComponent<Animator>();
            pm = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Movement>();
            sfx = GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update()
        {

            damaged = false;

        }

        public void TakeDamage(int amount)
        {
            damaged = true;

            currentHealth -= amount;
            //shows damage if player doesn't die
            if (currentHealth > 0)
            {
                anim.SetTrigger("Hurt");
                anim.SetTrigger("Idle");
                sfx.clip = playerHurt;
                sfx.Play();
            }

            //kills player if health reaches or exceeds 0
            if (currentHealth <= 0 && !isDead)
            {
                Death();
            }
        }

        void Death()
        {
            isDead = true;

            //plays death animation
            anim.SetTrigger("Die");
            pm.movement = false;
            sfx.clip = playerDeath;
            sfx.Play();
            Invoke("RestartLevel", 2.3f);

        }

        void RestartLevel()
        {
            SceneManager.LoadScene(0);
        }
    }
}
