﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace circus
{
    public class looseObject : MonoBehaviour
    {

        Rigidbody2D rb2d;
        public float fallDelay = .1f;
        Animator anim;
        PlayerHealth ph;
        bool hasFallen = false;
        AudioSource sfx;
        public AudioClip fallClip;
        // Start is called before the first frame update
        void Awake()
        {
            rb2d = GetComponent<Rigidbody2D>();
            anim = GetComponent<Animator>();
            ph = GameObject.FindGameObjectWithTag("PC").GetComponent<PlayerHealth>();
        }

        void OnCollisionEnter2D(Collision2D other)
        {
            //if the object is hit by a projectile and hasn't fallen yet...
            if (other.gameObject.CompareTag("Projectile") && rb2d.isKinematic && hasFallen == false)
            {
                anim.SetTrigger("Hit");
                //falls after a slight delay
                Invoke("Fall", fallDelay);
                anim.SetTrigger("Idle");
                //destroys RigidBody2D after 2 seconds have passed, allowing the object to fall through the floor
                Invoke("Fallen", 2f);
                hasFallen = true;
                sfx = GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>();
                sfx.clip = fallClip;
                sfx.Play();

            }

            if (other.gameObject.CompareTag("Player") && !rb2d.isKinematic)
            {
                //Deals 5 damage to the player when hit by a fallen object.
                ph.TakeDamage(5);
            }
            else if (other.gameObject.CompareTag("Enemy") && !rb2d.isKinematic)
            {
                //Deals 5 damage to enemies when hit by a fallen object.
                other.gameObject.GetComponent<EnemyHealth>().TakeDamage(5);
            }


        }

        void Fall()
        {
            // anim.SetTrigger("Hit");

            rb2d.isKinematic = false;
            //anim.SetTrigger("Idle");
        }

        void Fallen()
        {
            //rb2d.isKinematic = true;
            Destroy(GetComponent<BoxCollider2D>());
        }
    }
}
