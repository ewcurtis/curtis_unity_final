﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace circus
{
    public class CamBounds : MonoBehaviour
    {
        private Vector2 minBoundary;
        private Vector2 maxBoundary;

        private Transform cameraPos;
        private float ypos;

        private Transform player;

        // Start is called before the first frame update
        void Start()
        {
            minBoundary.x = 0;
            minBoundary.y = 0;
            //sets boudries for camera
            maxBoundary.x = 112.4172f;
            maxBoundary.y = 0;
            cameraPos = GameObject.FindGameObjectWithTag("MainCamera").transform;


        }

        // Update is called once per frame
        void Update()
        {
            //Prevents camera from going out of bounds
            cameraPos.position = new Vector3
                (
            Mathf.Clamp(cameraPos.position.x, minBoundary.x, maxBoundary.x),
            Mathf.Clamp(cameraPos.position.y, minBoundary.y, maxBoundary.y),
            cameraPos.position.z
            );

        }
    }
}
