﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace circus
{
    public class CardManagement : MonoBehaviour
    {

        public GameObject card;
        private GameObject player;
        Animator anim;
        private float cooldown = 1.30f;
        float timer;
        public GameObject CardManager;
        private GameObject cardClone;
        Animator cardAnim;
        bool facingRight = true;
        bool canFire = true;



        // Start is called before the first frame update
        void Start()
        {
            player = GameObject.FindGameObjectWithTag("PC");
            anim = player.GetComponent<Animator>();

            facingRight = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Movement>().facingRight;

        }

        // Update is called once per frame
        void Update()
        {
            if (player.GetComponent<PlayerHealth>().currentHealth <= 0)
            {
                canFire = false;
            }
            timer += Time.deltaTime;
            if (canFire)
            {
                if (Input.GetKey(KeyCode.E) && timer >= cooldown && Time.timeScale != 0 && facingRight)
                {
                    ThrowCardRight();
                }
                else if (Input.GetKey(KeyCode.E) && timer >= cooldown && Time.timeScale != 0 && !facingRight)
                {
                    ThrowCardLeft();

                }
            }


        }

        void ThrowCardRight()
        {
            timer = 0f;
            //begins animation for throwing card
            anim.SetTrigger("Shoot");

            //Invokes MakeCard after a delay to account for the animation
            Invoke("MakeCard", 0.6f);

            //prevents throwing animation from looping
            anim.SetTrigger("Idle");

        }

        void ThrowCardLeft()
        {
            timer = 0f;
            //begins animation for throwing card
            anim.SetTrigger("Shoot2");

            //Invokes MakeCard after a delay to account for the animation
            Invoke("MakeCard", 0.6f);

            //prevents throwing animation from looping
            anim.SetTrigger("Idle");

        }

        void MakeCard()
        {
            //Creates instance of card being thrown
            cardClone = Instantiate(card, CardManager.transform);
            //Destroys card after it exits screen
            Destroy(cardClone, 0.7f);

        }
    }
}
