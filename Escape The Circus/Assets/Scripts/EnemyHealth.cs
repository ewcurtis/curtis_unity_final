﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int startingHealth = 3;
    public int currentHealth;
    [HideInInspector]public bool damaged = false;
    [HideInInspector]
    bool isDead = false;
    Animator anim;
    AudioSource sfx;
    public AudioClip enemyHurt;
    public AudioClip enemyDeath;
    // Start is called before the first frame update
    void Awake()
    {
        currentHealth = startingHealth;
        anim = GetComponent<Animator>();
        sfx = GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (damaged)
        {
            Invoke("NoDamage", 1f);
        }
    }

    public void TakeDamage(int amount)
    {
        damaged = true;
        anim.SetTrigger("Hurt");
        currentHealth -= amount;
        anim.SetTrigger("Idle");
        sfx.clip = enemyHurt;
        sfx.Play();

        

        //kills player if health reaches or exceeds 0
        if (currentHealth <= 0 && !isDead)
        {
            Death();
            sfx.clip = enemyDeath;
            sfx.Play();
        }
    }

    void Death()
    {
        isDead = true;

        //plays death animation
        anim.SetTrigger("Die");
        Destroy(gameObject, 0.5f);
        //pm.movement = false;

        //Invoke("RestartLevel", 2.3f);

    }
    void NoDamage()
    {
        damaged = false;
    }
}
