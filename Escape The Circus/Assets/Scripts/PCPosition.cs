﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PCPosition : MonoBehaviour
{

    private Transform player;
    // Start is called before the first frame update
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = player.position;
        
    }
}
