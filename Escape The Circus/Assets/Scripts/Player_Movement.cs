﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace circus
{
    public class Player_Movement : MonoBehaviour
    {
        [HideInInspector]
        public bool facingRight = true;
        [HideInInspector]
        public bool jump = false;

        public float moveForce = 300f;
        public float maxSpeed = 5f;
        public float jumpForce = 300f;
        public Transform groundCheck;
        public GameObject player;

        private bool grounded = false;
        [HideInInspector]
        public bool movement = true;
        private SpriteRenderer sr;
        private Animator anim;
        private Rigidbody2D rb2D;
        private Transform currPos;
        // Start is called before the first frame update
        void Awake()
        {
            anim = player.GetComponent<Animator>();
            rb2D = GetComponent<Rigidbody2D>();
            sr = player.GetComponent<SpriteRenderer>();
            currPos = GetComponent<Transform>();

        }

        // Update is called once per frame
        void Update()
        {
            //currPos.localScale = new Vector3(player.GetComponent<Transform>().position.x, currPos.position.y, currPos.position.z);
            grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

            if (Input.GetButtonDown("Jump") && grounded && movement)
            {
                jump = true;
            }

        }

        void FixedUpdate()
        {
            if (movement)
            {
                float h = Input.GetAxis("Horizontal");
                anim.SetFloat("Run", Mathf.Abs(h));

                if (h * rb2D.velocity.x < maxSpeed)
                {
                    rb2D.AddForce(Vector2.right * h * moveForce);
                }
                //Makes sure we don't go too fast
                if (Mathf.Abs(rb2D.velocity.x) > maxSpeed)
                {
                    rb2D.velocity = new Vector2(Mathf.Sign(rb2D.velocity.x) * maxSpeed, rb2D.velocity.y);
                }
                //flips if the sprite isn't facing the same direction it's moving
                if (h > 0 && !facingRight)
                {
                    Flip();
                }
                else if (h < 0 && facingRight)
                {
                    Flip();
                }
                //jumps if player presses the jump button
                if (jump)
                {
                    anim.SetTrigger("Jump");
                    rb2D.AddForce(new Vector2(0f, jumpForce));
                    jump = false;
                }
            }
        }

        void Flip()
        {

            facingRight = !facingRight;
            Vector3 theScale = sr.transform.localScale;
            theScale.x *= -1;
            sr.transform.localScale = theScale;
        }


    }
}
