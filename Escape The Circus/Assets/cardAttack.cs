﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace circus
{
    public class cardAttack : MonoBehaviour
    {
        bool hitEnemy = false;
        public int dmg = 1;
        public float cooldown = 1f;
        EnemyHealth eh;
        float timer;
        Collider2D hitBox;
        public GameObject[] enemies;
        Collider2D collider;
        Rigidbody2D rb2d;
        bool playerDirection;


        // Start is called before the first frame update
        void Awake()
        {
            hitBox = GetComponent<Collider2D>();
            enemies = GameObject.FindGameObjectsWithTag("Enemy");
            rb2d = GetComponent<Rigidbody2D>();
            playerDirection = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Movement>().facingRight;
        }

        // Update is called once per frame
        void Update()
        {
            timer += Time.deltaTime;
            if (hitEnemy == true && timer % 2 == 0)
            {
                hitEnemy = false;

            }

        }

        void OnTriggerEnter2D(Collider2D other)
        {

            if (other.gameObject.CompareTag("Enemy") && hitEnemy == false)
            {
                hitEnemy = true;
                other.GetComponent<EnemyHealth>().TakeDamage(dmg);

            }
            /*else if (other.gameObject.CompareTag("Hazard"))
            {
                Destroy(gameObject);
            }*/

        }

        void OnTriggerExit2D(Collider2D other)
        {

            if (other.gameObject.CompareTag("Enemy"))
            {

                hitEnemy = false;
                Destroy(gameObject);

            }

        }

        void Attack()
        {
            // Reset the timer.
            //timer = 0f;

            for (int i = 0; i <= enemies.Length; i++)
            {

                if (hitBox.IsTouching(enemies[i].GetComponent<EdgeCollider2D>()))
                {
                    eh = enemies[i].GetComponent<EnemyHealth>();

                    // If the enemy has health to lose...
                    if (eh.currentHealth > 0)
                    {
                        // ... damage the enemy.
                        eh.TakeDamage(dmg);
                    }

                }

            }

            //eh = hitBox.GetComponent<EnemyHealth>();

        }


    }
}