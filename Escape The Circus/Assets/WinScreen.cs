﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace circus
{
    public class WinScreen : MonoBehaviour
    {
        
        Animator anim;
        // Start is called before the first frame update
        void Awake()
        {

            anim = GetComponent<Animator>();
            
        }

        // Update is called once per frame
        void Update()
        {

            

            if (WinGame.win == true)
            {
                
                anim.SetTrigger("win");
                Invoke("Restart", 3f);
                WinGame.win = false;
            }

        }

        void Restart()
        {
            SceneManager.LoadScene(0);
        }
    }
}