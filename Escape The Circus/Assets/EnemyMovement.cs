﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{

    Transform enemypos;
    public float maxPosx = 12f;
    public float minPosx = 3f;
    //int direction = 0;
    bool facingRight = false;
    float newPosx;
    EnemyHealth eh;

    // Start is called before the first frame update
    void Awake()
    {
        enemypos = GetComponent<Transform>();
        newPosx = enemypos.position.x;
        eh = GetComponent<EnemyHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        //Enemy can only move if he isn't dead.
        if (eh.currentHealth > 0 && !eh.damaged)
        {
            //moving left
            if (newPosx > minPosx && facingRight == false)
            {
                newPosx -= 0.1f;
            }
            //turning right
            else if (newPosx <= minPosx && facingRight == false)
            {
                Flip();
            }
            //moving right
            else if (newPosx < maxPosx && facingRight == true)
            {
                newPosx += 0.1f;
            }
            //turning left
            else if (newPosx >= maxPosx && facingRight == true)
            {
                Flip();
            }

            //adjusts enemy position
            enemypos.localPosition = new Vector3(newPosx, enemypos.position.y, enemypos.position.z);
        }
        
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = enemypos.transform.localScale;
        theScale.x *= -1;
        enemypos.transform.localScale = theScale;
    }
}
