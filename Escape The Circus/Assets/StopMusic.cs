﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace  circus {
public class StopMusic : MonoBehaviour
{

    AudioSource music;
    // Start is called before the first frame update
    void Awake()
    {
        music = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //stops normal music when player wins game
        if (WinGame.win)
        {
            music.Stop();

        }
    }
}
}
