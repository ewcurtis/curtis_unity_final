﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace circus
{
    public class WinGame : MonoBehaviour
    {

        public static bool win = false;
        GameObject player;
        AudioSource winMusic;
        public AudioClip winClip;

        // Start is called before the first frame update
        void Awake()
        {
            player = GameObject.FindGameObjectWithTag("Player");
            winMusic = player.GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update()
        {
            if (win)
            {
                winMusic.clip = winClip;
                winMusic.Play();
            }
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            //triggers win condition if player completes level
            if (other.gameObject == player)
            {
                
                win = true;
                
            }
        }


    }
}